#ifndef STR_CALCULATOR_H
#define STR_CALCULATOR_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <sstream>
#include <stdexcept>
#include <utility>

class Str_Calculator
{
    private:
        std::string holder;
    private:
        std::string compute(std::string& str);
        std::string compute_priority(std::string& str);
        std::pair<double, int> find_operand(bool x, int index, std::string& str);
        double get_value(std::vector<char>& chars);
        std::pair<int, char> find_punct(std::string& str);
        std::string make_string(double& str);
        std::string search_for_parenthesis(std::string& str);
    public:
        Str_Calculator() = default;
        Str_Calculator(const Str_Calculator& calc) = default;
        Str_Calculator(Str_Calculator&& calc) noexcept = default;
        virtual ~Str_Calculator() = default;
        Str_Calculator& operator=(const Str_Calculator& calc) = default;
        Str_Calculator& operator=(Str_Calculator&& calc) noexcept = default;
        Str_Calculator(const std::string& str) : holder(str) {}
        std::string& getHolder() {return holder;}
        void setHolder(const std::string& str) {holder = str;}
        std::string operator()(const std::string& str);
        std::string operator()();
};

std::string Str_Calculator::compute(std::string& str)
{
	// find index of punctuation!
	auto punct = find_punct(str);
	double opA = find_operand(true, punct.first, str).first;
	double opB = find_operand(false, punct.first, str).first;

	double result{};
	switch (punct.second)
	{
		case '+':
			result = opA + opB;
			break;
		case '-':
			result = opA - opB;
			break;
		case '*':
			result = opA * opB;
			break;
		case '/':
			result = opA / opB;
			break;
		default:
			throw std::logic_error("No operation found.");
	}
	std::string res_string(make_string(result));
	return res_string;
}

std::string Str_Calculator::compute_priority(std::string& str)
{
	std::string result(str);
	int mult = result.find('*');
	int div = result.find('/');
	int first = -1;
	if (mult != std::string::npos && div != std::string::npos)
		first = (mult < div) ? mult : div;
	else if (mult != std::string::npos && div == std::string::npos)
		first = mult;
	else if (mult == std::string::npos && div != std::string::npos)
		first = div;
	else
	{
		while(!ispunct(result[++first]) || result[first] == '.')
		{
			if (first >= result.size())
				return result;
		}
	}
	//std::cout << "place of dividing pivot: " << first << std::endl;

	double leftBound = find_operand(true, first, result).second;
	double rightBound = find_operand(false, first, result).second;

	std::string cut(result.begin()+leftBound, result.begin()+rightBound);

	//std::cout << "cut: " << cut << std::endl;

	std::string cut_computed(compute(cut));

	//std::cout << "computed cut: " << cut_computed << std::endl;

	result.erase(result.begin()+leftBound, result.begin()+rightBound);

	//std::cout << "result erased: " << result;

	result.insert(leftBound, cut_computed);

	//std::cout << "result insert: " << result << std::endl;

	return compute_priority(result);
}

std::pair<double, int> Str_Calculator::find_operand(bool x, int index, std::string& str) // finds operand for the operation
{
	using std::vector;
	typedef int BoundaryIndex;
	typedef double Operand;
	std::pair<Operand, BoundaryIndex> valueToReturn; 
	if (x == true) // search to the left
	{
		vector<char> chars;

		while (!isdigit(str[index])) // go left until you find digit
		{
			--index;
			if (isdigit(str[index]))
			{
				chars.push_back(str[index]);
				// Doesn't recognise the dot and does not work with it!
				while (isdigit(str[--index]) && index >= 0) // go left until the end or until you find digits and push them back into vector
					chars.push_back(str[index]);
				if (str[index] == '.') // after a dot, do the same
				{
					chars.push_back(str[index]);
					while (isdigit(str[--index]) && index >= 0)
					{
						chars.push_back(str[index]);
					}
				}
				break; 
			}
		}
		std::reverse(chars.begin(), chars.end()); // reverse because it is the other way round it should be
		
		valueToReturn.second = index + 1;
		valueToReturn.first = get_value(chars); // to check

		//std::cout << "compute_priority\n";
		//for (auto& i: chars) std::cout << i;
		//std::cout << std::endl;

		return valueToReturn;
	}
	else // search to the right
	{
		vector<char> chars;

		while (!isdigit(str[index])) // go right until you find digit
		{
			++index;
			if (isdigit(str[index]))
			{
				chars.push_back(str[index]);
				while (isdigit(str[++index]) && index < str.size()) // go right until the end or until you find digits and push them back into vector
					chars.push_back(str[index]);
					//valueToReturn.second = index;
				if (str[index] == '.') // after a dot, do the same
				{
					chars.push_back(str[index]);
					while (isdigit(str[++index]) && index < str.size())
					{
						chars.push_back(str[index]);
					}
				}
				break; 
			}
		}
		// no need for reversing 'cause it was going the the right
		valueToReturn.first = get_value(chars);
		valueToReturn.second = index;
		
		//std::cout << "compute_priority\n";
		//for (auto& i: chars) std::cout << i;
		//std::cout << std::endl;

		return valueToReturn;
	}
	throw std::logic_error("This should have never happened!");
}

double Str_Calculator::get_value(std::vector<char>& chars)
{
	std::string result;
	result.resize(chars.size());
	std::copy(chars.begin(), chars.end(), result.begin());
	std::stringstream stream(result);
	double res_int{};
	stream >> res_int;
	return res_int;
}

std::pair<int, char> Str_Calculator::find_punct(std::string& str)
{
	std::pair<int, char> result;
	//std::cout << "Searching through: " << str << " of size: " << str.size() << std::endl;
	for (int i = 0; i < str.size(); ++i)
	{
		//std::cout << "sign: " << str[i] << " position: " << i << std::endl;
		if (ispunct(str[i]) && str[i] != '.')
		{
			//std::cout << "sign located: " << str[i] << " position: " << i << std::endl;
			result.first = i;
			result.second = str[i];
			break;
		}
	}
	return result;
}

std::string Str_Calculator::make_string(double& num)
{
	std::string result;
	std::stringstream stream;
	stream << num;
	stream >> result;
	return result;
}

std::string Str_Calculator::search_for_parenthesis(std::string& str)
{
	std::string result(str);
	int leftParen = result.find('(');
	if (leftParen != std::string::npos) // if there is a param
	{
		int innerLeftParen{};
		while (leftParen!=std::string::npos) // find the rightmost param or the most inner one
		{
			innerLeftParen = leftParen;
			leftParen = str.find('(', leftParen+1);
		}
		leftParen = innerLeftParen;
		int rightParen = result.find(')', leftParen);

		std::string operation_in_params(result.begin()+leftParen+1, result.begin()+rightParen);

		operation_in_params = compute_priority(operation_in_params);

		result.erase(result.begin()+leftParen, result.begin()+rightParen+1);

		result.insert(leftParen, operation_in_params);

		return search_for_parenthesis(result);
	}
	return result;
}

std::string Str_Calculator::operator()(const std::string& str)
{
	std::string result(str);
	result = search_for_parenthesis(result);
	//std::cout << "After parenthesis: " << result << std::endl;
	return compute_priority(result);
}

std::string Str_Calculator::operator()()
{
    if (holder.size() != 0)
    {
        std::string result(holder);
        result = search_for_parenthesis(result);
	//	std::cout << "After parenthesis: " << result << std::endl;
        return compute_priority(result);
    }
    throw std::logic_error("Empty holder");
}

#endif // STR_CALCULATOR_H

